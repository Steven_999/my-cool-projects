﻿using System;
using System.IO;

namespace ConsoleApp23
{
    class Program
    {
        enum KeuzeMenus
        {
            Reservatieaanmaken,
            Reservatiewijzigen,
            Reservatieverwijderen,
            Reserveringsoverzicht,
            Tafelbeheer
        }
        enum KeuzeSubMenuTafels
        {
            Tafelaanmaken,
            Tafeloverzicht,
            Hoofdmenu
        }

        static void Main(string[] args)
        {
            try
            {
                MainMenu();
            }
            catch (Exception)
            {
                Console.WriteLine("Sorry, er is iets misgegaan. Start de applicatie opnieuw op.");
                throw;
            }
        }

        // Hoofdmenu
        static void MainMenu()
        {
            Console.Clear();

            string logo =
            @"
  _______  _______  _______ _________ _______  _______  _______  _______          _________ _______  _______
 (  ____ )(  ____ \(  ____ \\__   __/(  ___  )(  ____ \(  ____ \(  ____ )|\     /|\__   __/(  ____ \(  ____ \
 | (    )|| (    \/| (    \/   ) (   | (   ) || (    \/| (    \/| (    )|| )   ( |   ) (   | (    \/| (    \/
 | (____)|| (__    | (_____    | |   | |   | || (_____ | (__    | (____)|| |   | |   | |   | |      | (__
 |     __)|  __)   (_____  )   | |   | |   | |(_____  )|  __)   |     __)( (   ) )   | |   | |      |  __)
 | (\ (   | (            ) |   | |   | |   | |      ) || (      | (\ (    \ \_/ /    | |   | |      | (
 | ) \ \__| (____/\/\____) |   | |   | (___) |/\____) || (____ /\| ) \ \__ \   /  ___) (___| (____ /\| (____ /\
 |/   \__ / (_______ /\_______)   )_((_______)\_______)(_______ /|/   \__ / \_/   \_______/(_______ /(_______ /
           ";

            Console.WriteLine(logo + Environment.NewLine);



            string[] hoofdMenu = { "Reservatie aanmaken", "Reservatie wijzigen", "Reservatie verwijderen", "Reserveringsoverzicht", "Tafelbeheer" };
            int keuzeMenu;

            Console.WriteLine("Gelieve uw keuze te maken uit onderstaande: (Druk het bijpassende cijfer in)" + Environment.NewLine);

            for (int i = 0; i < hoofdMenu.Length; i++)
            {
                Console.WriteLine((i + 1) + ") " + hoofdMenu[i]);
            }

            while (!int.TryParse(Console.ReadLine(), out keuzeMenu))
            {
                Console.WriteLine("Graag een correct cijfer in te geven.");
            }

            while (keuzeMenu > hoofdMenu.Length)
            {
                Console.WriteLine("Graag een keuze maken uit de bestaande cijfers");

                while (!int.TryParse(Console.ReadLine(), out keuzeMenu))
                {
                    Console.WriteLine("Graag een correct cijfer in te geven.");
                }
            }

            switch (keuzeMenu)
            {

                case (int)KeuzeMenus.Reservatieaanmaken + 1:
                    ReservatieAanmaken();
                    break;

                case (int)KeuzeMenus.Reservatiewijzigen + 1:
                    Reservatiewijzigen();
                    break;
                case (int)KeuzeMenus.Reservatieverwijderen + 1:
                    Reservatieverwijderen();
                    break;
                case (int)KeuzeMenus.Reserveringsoverzicht + 1:
                    Reserveringsoverzicht();
                    break;
                case (int)KeuzeMenus.Tafelbeheer + 1:
                    TafelBeheer();
                    break;

                default:
                    break;
            }

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ReadLine();

        }
        static void ReservatieAanmaken()
        {
            DateTime reservatieDag = new DateTime();
            DateTime vandaag = DateTime.Now;
            DateTime eindeVanHetJaar = new DateTime(DateTime.Today.Year + 1, 1, 1).AddDays(-1);
            DateTime[] alleResteredenDagen = new DateTime[0];
            string keuzeLunchDiner = "";
            string aantalPersoon;
            string naamReservatie;
            string dag;
            string[] ingelezenFile = { "" };
            string[] tafelNaam = new string[1];
            string[] aantalTafels = { };
            string[] aantalPersonenKeuze = { };
            string ingelezenLijn = "";
            string[] ingelezenKeuzeLijn = { };
            int teWijzigenLijn = -1;
            int keuzeAantalpersoon;
            string fileName = "BezettingperDatum.txt";
            string toegewezenTafel = "";

            Console.Clear();

            Console.WriteLine("Op welke naam mag uw reservatie staan?");
            naamReservatie = Console.ReadLine();

            Console.WriteLine(Environment.NewLine + "Op welke datum wilt u een reservatie aanmaken? (dd/mm/yyyy)");

            while (!DateTime.TryParse(Console.ReadLine(), out reservatieDag))
            {
                Console.WriteLine("Graag in het format dd/mm/yyyy).");
            }

            while (reservatieDag.CompareTo(vandaag) <= 0)
            {
                Console.WriteLine("De ingegeven datum ligt in het verleden. Gelieve opnieuw een datum in te geven.");
                while (!DateTime.TryParse(Console.ReadLine(), out reservatieDag))
                {
                    Console.WriteLine("Graag in het format dd/mm/yyyy.");
                }
            }
            dag = reservatieDag.ToString("dd/MM/yyyy");

            Console.WriteLine(Environment.NewLine + "Voor hoeveel personen wilt u reserveren?");

            while (!int.TryParse(Console.ReadLine(), out keuzeAantalpersoon))
            {
                Console.WriteLine("Gelieve een cijfer in te geven voor het aantal personen.");
            }

            while (keuzeAantalpersoon > 8)
            {
                Console.WriteLine(Environment.NewLine + "Gelieve te bellen naar het restaurant als uw aantal personen groter is dan 8.");
                Console.ReadLine();
                MainMenu();
            }

            aantalPersoon = keuzeAantalpersoon.ToString();

            if (aantalPersoon == "1")
            {
                aantalPersoon = "2";
            }
            if (aantalPersoon == "3")
            {
                aantalPersoon = "4";
            }
            if (aantalPersoon == "5")
            {
                aantalPersoon = "6";
            }
            if (aantalPersoon == "7")
            {
                aantalPersoon = "8";
            }

            switch (aantalPersoon)
            {
                case "2":
                    aantalPersoon = "2";
                    Tafelkeuze(ref dag, ref aantalPersoon, ref teWijzigenLijn, ref ingelezenLijn, ref toegewezenTafel);
                    break;
                case "4":
                    aantalPersoon = "4";
                    Tafelkeuze(ref dag, ref aantalPersoon, ref teWijzigenLijn, ref ingelezenLijn, ref toegewezenTafel);
                    break;
                case "6":
                    aantalPersoon = "6";
                    Tafelkeuze(ref dag, ref aantalPersoon, ref teWijzigenLijn, ref ingelezenLijn, ref toegewezenTafel);
                    break;
                case "8":
                    aantalPersoon = "8";
                    Tafelkeuze(ref dag, ref aantalPersoon, ref teWijzigenLijn, ref ingelezenLijn, ref toegewezenTafel);
                    break;

                default:
                    break;
            }

            if (teWijzigenLijn > -1)
            {
                lineChanger(ingelezenLijn, fileName, teWijzigenLijn);
            }
            else
            {
                Console.WriteLine("Er zijn geen tafels meer vrij voor het aantal personen." + Environment.NewLine +
                                                            "Gelieve contact op te nemen met het restaurant" + Environment.NewLine +
                                                            "Druk op enter om terug te keren naar het hoofdmenu.");
                Console.ReadLine();
            }


            //Console.WriteLine(Environment.NewLine + "1) Wilt u voor de lunch reserveren, druk 1." + Environment.NewLine + "2) Wilt u voor het diner reserveren, druk 2");

            //while (!int.TryParse(Console.ReadLine(), out antwoordLunchDiner))
            //{
            //    Console.WriteLine("Graag het cijfer 1 of 2 invoeren.");
            //}
            //while (antwoordLunchDiner > 2)
            //{
            //    Console.WriteLine("u heeft een cijfer groter dan 2 ingegeven. Gelieve opnieuw een keuze te maken.");
            //    while (!int.TryParse(Console.ReadLine(), out antwoordLunchDiner))
            //    {
            //        Console.WriteLine("Graag het cijfer 1 of 2 invoeren.");
            //    }
            //}

            //if (antwoordLunchDiner == 1)
            //{
            //    keuzeLunchDiner = "lunch";
            //}
            //else
            //{
            //    keuzeLunchDiner = "diner";
            //}

            string Namen = "";
            string ReservatieDagen = "";
            string KeuzeLunchDiner = "";
            string AantalPersonen = "";
            string toegewezenTafels = "";

            if (teWijzigenLijn > -1)
            {
                if (File.Exists("Reservaties.txt"))
                {
                    InlezenReservatieBestand2(ref Namen, ref ReservatieDagen, ref KeuzeLunchDiner, ref AantalPersonen, ref toegewezenTafels);
                }

                string tussenTeken = ";";
                fileName = "Reservaties.txt";

                using (StreamWriter writer = new StreamWriter(fileName))
                {
                    if (Namen == null || Namen == "")
                    {
                        tussenTeken = "";
                    }

                    writer.WriteLine(Namen + tussenTeken + naamReservatie);
                    writer.WriteLine(ReservatieDagen + tussenTeken + dag);
                    writer.WriteLine(KeuzeLunchDiner + tussenTeken + keuzeLunchDiner);
                    writer.WriteLine(AantalPersonen + tussenTeken + aantalPersoon);
                    writer.WriteLine(toegewezenTafels + tussenTeken + toegewezenTafel);
                }
            }

            Console.Clear();
            MainMenu();

        }
        static void InlezenReservatieBestand2(ref string dataNamen, ref string dataReservatieDagen, ref string dataKeuzeLunchDiners, ref string dataAantalPersonen, ref string dataToeGewezenTafels)
        {
            using (StreamReader reader = new StreamReader("Reservaties.txt"))
            {
                dataNamen = reader.ReadLine();
                dataReservatieDagen = reader.ReadLine();
                dataKeuzeLunchDiners = reader.ReadLine();
                dataAantalPersonen = reader.ReadLine();
                dataToeGewezenTafels = reader.ReadLine();
            }
        }
        static void Reservatiewijzigen()
        {
            string Namen = "";
            string ReservatieDagen = "";
            string KeuzeLunchDiner = "";
            string AantalPersonen = "";
            string toegewezenTafels = "";
            int keuzeWijziging;
            string[] ingelezenFile = new string[1];
            int teWijzigenLijn = -1;
            int teWijzigenLijnOrigineel = 0;
            string ingelezenLijn = "";
            string matchDatum = "";
            string dag = "";
            DateTime vandaag = DateTime.Now;
            DateTime reservatieDag = new DateTime();
            string[] ingelezenReservaties = { };
            string namenReservaties = "";
            int tellerNamen = 1;
            string aantalTafelsOpDatum = "";
            int lengteSubtring = 0;
            int totaleLengsteString = 0;
            string[] arrAantalTafelsopDatum = { };
            int antwoordWijziging;
            int tellerLijn = 0;
            string fileName = "BezettingperDatum.txt";
            int tellerVerwijderdeTafel = 0;
            string aantalTafelsOpDatumNieuw = "";
            string[] arrAantalTafelsOpDatumNieuw = { };

            Console.Clear();

            if (File.Exists("Reservaties.txt"))
            {

                ingelezenFile[0] = File.ReadAllText("Reservaties.txt");

                if (string.IsNullOrWhiteSpace(ingelezenFile[0]))
                {
                    Console.WriteLine("Er zijn geen reserveringen om te wijzigen." + Environment.NewLine + "Druk op enter om terug te keren naar het hoofdmenu.");
                    Console.ReadLine();
                    MainMenu();
                }

                InlezenReservatieBestand2(ref Namen, ref ReservatieDagen, ref KeuzeLunchDiner, ref AantalPersonen, ref toegewezenTafels);
            }
            else
            {
                Console.WriteLine("Er zijn geen reserveringen om te wijzigen." + Environment.NewLine + "Druk op enter om terug te keren naar het hoofdmenu.");
                Console.ReadLine();
                MainMenu();
            }

            string[] arrNamen = Namen.Split(';', Namen.Length);
            string[] arrReservatieDagen = ReservatieDagen.Split(';', ReservatieDagen.Length);
            string[] arrKeuzeLunchDiner = KeuzeLunchDiner.Split(';', KeuzeLunchDiner.Length);
            string[] arrAantalPersonen = AantalPersonen.Split(';', AantalPersonen.Length);
            string[] arrToegewezenTafels = toegewezenTafels.Split(';', toegewezenTafels.Length);

            Console.WriteLine("Op welke datum wilt u een reservatie wijzigen? (dd/mm/yyyy)");

            while (!DateTime.TryParse(Console.ReadLine(), out reservatieDag))
            {
                Console.WriteLine("Graag in het format dd/mm/yyyy).");
            }

            while (reservatieDag.CompareTo(vandaag) < 0)
            {
                Console.WriteLine("De ingegeven datum ligt in het verleden. Gelieve opnieuw een datum in te geven.");
                while (!DateTime.TryParse(Console.ReadLine(), out reservatieDag))
                {
                    Console.WriteLine("Graag in het format dd/mm/yyyy.");
                }
            }

            dag = reservatieDag.ToString("dd/MM/yyyy");

            using (StreamReader reader = new StreamReader("BezettingperDatum.txt"))
            {
                teWijzigenLijn = 0;
                for (int j = 0; j < File.ReadAllLines("BezettingperDatum.txt").Length; j++)
                {
                    ingelezenLijn = reader.ReadLine();
                    matchDatum = ingelezenLijn;
                    teWijzigenLijnOrigineel++;

                    if (!(ingelezenLijn.IndexOf(';') == -1))
                    {
                        matchDatum = ingelezenLijn.Substring(0, ingelezenLijn.IndexOf(";"));
                    }
                    if (matchDatum == dag)
                    {
                        break;
                    }
                }
            }

            aantalTafelsOpDatum = ingelezenLijn;

            if (!(aantalTafelsOpDatum == matchDatum))
            {
                lengteSubtring = Convert.ToInt32(aantalTafelsOpDatum.Substring(0, aantalTafelsOpDatum.IndexOf(';')).Length + 1);
                totaleLengsteString = aantalTafelsOpDatum.Length - lengteSubtring;
                aantalTafelsOpDatum = aantalTafelsOpDatum.Substring(lengteSubtring, totaleLengsteString);
                arrAantalTafelsopDatum = aantalTafelsOpDatum.Split(";");
            }
            else
            {
                Console.WriteLine(Environment.NewLine + "Er zijn geen reserveringen om te wijzigen." + Environment.NewLine + "Druk op enter om terug te keren naar het hoofdmenu.");
                Console.ReadLine();
                MainMenu();
            }

            Console.Clear();

            Console.WriteLine("Welke reservering wilt u wijzigen?" + Environment.NewLine);

            for (int i = 0; i < arrAantalTafelsopDatum.Length; i++)
            {
                for (int j = 0; j < arrAantalPersonen.Length; j++)
                {
                    if ((arrAantalTafelsopDatum[i] == arrToegewezenTafels[j]) && (dag == arrReservatieDagen[j]))
                    {
                        namenReservaties += tellerNamen + ") " + arrNamen[j].PadRight(12) + " " + arrAantalPersonen[j] + "P" + Environment.NewLine;
                        tellerNamen++;
                    }
                }
            }

            Console.WriteLine(namenReservaties);

            while (!int.TryParse(Console.ReadLine(), out antwoordWijziging))
            {
                Console.WriteLine("Graag een cijfer ingeven.");
            }
            while (antwoordWijziging > arrNamen.Length)
            {
                Console.WriteLine("Graag een juiste keuzemenu aanduiden.");
                while (!int.TryParse(Console.ReadLine(), out antwoordWijziging))
                {
                    Console.WriteLine("Graag een cijfer ingeven.");
                }
            }

            Console.Clear();

            Console.WriteLine("Wat wilt u juist wijzigen?" + Environment.NewLine);

            for (int i = 0; i < arrAantalTafelsopDatum.Length; i++)
            {
                for (int j = 0; j < arrAantalPersonen.Length; j++)
                {
                    if ((arrAantalTafelsopDatum[antwoordWijziging - 1] == arrToegewezenTafels[j]) && (dag == arrReservatieDagen[j]))
                    {
                        Console.WriteLine("1) ReservatieNaam: " + arrNamen[j] + Environment.NewLine + "2) Reservatiedatum: " + arrReservatieDagen[j] + Environment.NewLine + "3) Aantal personen: " + arrAantalPersonen[j]);
                        break;
                    }
                    tellerLijn++;
                }
                break;
            }


            while (!int.TryParse(Console.ReadLine(), out keuzeWijziging))
            {
                Console.WriteLine("Graag een cijfer ingeven.");
            }
            while (keuzeWijziging > 3)
            {
                Console.WriteLine("Graag een juiste keuzemenu aanduiden.");
                while (!int.TryParse(Console.ReadLine(), out keuzeWijziging))
                {
                    Console.WriteLine("Graag een cijfer ingeven.");
                }
            }

            switch (keuzeWijziging)
            {
                case 1:

                    Console.WriteLine(Environment.NewLine + "Op welke naam mag uw reservatie staan?");
                    arrNamen[tellerLijn] = Console.ReadLine();
                    break;

                case 2:

                    Console.WriteLine(Environment.NewLine + "Naar welke datum wilt u uw reservatie wijzigen? (dd/mm/yyyy)");

                    while (!DateTime.TryParse(Console.ReadLine(), out reservatieDag))
                    {
                        Console.WriteLine("Graag in het format dd/mm/yyyy).");
                    }

                    while (reservatieDag.CompareTo(vandaag) < 0)
                    {
                        Console.WriteLine("De ingegeven datum ligt in het verleden. Gelieve opnieuw een datum in te geven.");
                        while (!DateTime.TryParse(Console.ReadLine(), out reservatieDag))
                        {
                            Console.WriteLine("Graag in het format dd/mm/yyyy.");
                        }
                    }
                    dag = reservatieDag.ToString("dd/MM/yyyy");

                    string aantalPersoon = arrAantalPersonen[tellerLijn];
                    string toegewezenTafel = arrToegewezenTafels[tellerLijn];

                    Tafelkeuze(ref dag, ref aantalPersoon, ref teWijzigenLijn, ref ingelezenLijn, ref toegewezenTafel);

                    if (teWijzigenLijn > -1)
                    {
                        aantalTafelsOpDatumNieuw = arrReservatieDagen[tellerLijn];

                        for (int i = 0; i < arrAantalTafelsopDatum.Length; i++)
                        {
                            if (arrToegewezenTafels[tellerLijn] == arrToegewezenTafels[tellerVerwijderdeTafel])
                            {
                                tellerVerwijderdeTafel++;
                            }
                            else
                            {
                                aantalTafelsOpDatumNieuw += ";" + arrToegewezenTafels[tellerVerwijderdeTafel];
                                tellerVerwijderdeTafel++;
                            }

                        }

                    }

                    if (teWijzigenLijn > -1)
                    {
                        lineChanger(ingelezenLijn, fileName, teWijzigenLijn);
                        lineChanger(aantalTafelsOpDatumNieuw, fileName, teWijzigenLijnOrigineel);
                        arrReservatieDagen[tellerLijn] = reservatieDag.ToString("dd/MM/yyyy");
                        arrToegewezenTafels[tellerLijn] = toegewezenTafel;
                    }
                    else
                    {
                        Console.WriteLine(Environment.NewLine + "Er zijn geen tafels meer vrij voor het aantal personen." + Environment.NewLine +
                                               "Gelieve contact op te nemen met het restaurant" + Environment.NewLine +
                                               "Druk op enter om terug te keren naar het hoofdmenu.");
                        Console.ReadLine();
                    }

                    break;

                case 3:

                    int anwoordAantalPersoon;

                    Console.WriteLine(Environment.NewLine + "Met hoeveel personen wilt u reserveren? Max 8 personen");

                    while (!int.TryParse(Console.ReadLine(), out anwoordAantalPersoon))
                    {
                        Console.WriteLine("Graag een cijfer invoeren");
                    }
                    while (anwoordAantalPersoon > 8)
                    {
                        Console.WriteLine("U kan max. met 8 personen reserveren. Gelieve te bellen als u met meer dan 8 personen wilt reserveren.");

                        while (!int.TryParse(Console.ReadLine(), out anwoordAantalPersoon))
                        {
                            Console.WriteLine("Graag een cijfer invoeren");
                        }
                    }

                    aantalPersoon = anwoordAantalPersoon.ToString();

                    if (aantalPersoon == "1")
                    {
                        aantalPersoon = "2";
                    }
                    if (aantalPersoon == "3")
                    {
                        aantalPersoon = "4";
                    }
                    if (aantalPersoon == "5")
                    {
                        aantalPersoon = "6";
                    }
                    if (aantalPersoon == "7")
                    {
                        aantalPersoon = "8";
                    }

                    toegewezenTafel = arrToegewezenTafels[tellerLijn];

                    Tafelkeuze(ref dag, ref aantalPersoon, ref teWijzigenLijn, ref ingelezenLijn, ref toegewezenTafel);

                    if (teWijzigenLijn > -1)
                    {
                        arrAantalTafelsOpDatumNieuw = ingelezenLijn.Split(";");
                        aantalTafelsOpDatumNieuw = arrReservatieDagen[tellerLijn];

                        for (int i = 0; i < arrAantalTafelsOpDatumNieuw.Length; i++)
                        {
                            if (arrToegewezenTafels[tellerLijn] == arrAantalTafelsOpDatumNieuw[i])
                            {
                                tellerVerwijderdeTafel++;
                            }
                            else
                            {
                                if (arrAantalTafelsOpDatumNieuw[i] == arrReservatieDagen[tellerLijn])
                                {
                                    tellerVerwijderdeTafel++;
                                }
                                else
                                {
                                    aantalTafelsOpDatumNieuw += ";" + arrAantalTafelsOpDatumNieuw[tellerVerwijderdeTafel];
                                    tellerVerwijderdeTafel++;
                                }

                            }

                        }

                    }

                    if (teWijzigenLijn > -1)
                    {
                        lineChanger(ingelezenLijn, fileName, teWijzigenLijn);
                        lineChanger(aantalTafelsOpDatumNieuw, fileName, teWijzigenLijnOrigineel);
                        arrAantalPersonen[tellerLijn] = anwoordAantalPersoon.ToString();
                        arrToegewezenTafels[tellerLijn] = toegewezenTafel;
                    }


                    break;

                case 4:

                //int antwoordLunchDiner;
                //string keuzeLunchDiner = "";

                //Console.WriteLine(Environment.NewLine + "1) Wilt u voor de lunch reserveren, druk 1." + Environment.NewLine + "2) Wilt u voor het diner reserveren, druk 2");

                //while (!int.TryParse(Console.ReadLine(), out antwoordLunchDiner))
                //{
                //    Console.WriteLine("Graag het cijfer 1 of 2 invoeren.");
                //}
                //while (antwoordLunchDiner > 2)
                //{
                //    Console.WriteLine("u heeft een cijfer groter dan 2 ingegeven. Gelieve opnieuw een keuze te maken.");
                //    while (!int.TryParse(Console.ReadLine(), out antwoordLunchDiner))
                //    {
                //        Console.WriteLine("Graag het cijfer 1 of 2 invoeren.");
                //    }
                //}

                //if (antwoordLunchDiner == 1)
                //{
                //    keuzeLunchDiner = "lunch";
                //}
                //else
                //{
                //    keuzeLunchDiner = "diner";
                //}
                //arrKeuzeLunchDiner[keuzeReservatie - 1] = keuzeLunchDiner;
                //break;

                default:
                    break;
            }

            Namen = string.Join(";", arrNamen);
            ReservatieDagen = string.Join(";", arrReservatieDagen);
            KeuzeLunchDiner = string.Join(";", arrKeuzeLunchDiner);
            AantalPersonen = string.Join(";", arrAantalPersonen);
            toegewezenTafels = string.Join(";", arrToegewezenTafels);

            fileName = "Reservaties.txt";

            using (StreamWriter writer = new StreamWriter(fileName))
            {
                writer.WriteLine(Namen);
                writer.WriteLine(ReservatieDagen);
                writer.WriteLine(KeuzeLunchDiner);
                writer.WriteLine(AantalPersonen);
                writer.WriteLine(toegewezenTafels);
            }
            Console.Clear();
            MainMenu();

        }
        static void Reservatieverwijderen()
        {
            string Namen = "";
            string ReservatieDagen = "";
            string KeuzeLunchDiner = "";
            string AantalPersonen = "";
            string toegewezenTafels = "";
            int keuzeVerwijdering = 0;
            string[] ingelezenFile = new string[1];
            DateTime reservatieDag;
            int teWijzigenLijn = -1;
            int teWijzigenLijnOrigineel = 0;
            string ingelezenLijn = "";
            string matchDatum = "";
            string dag = "";
            DateTime vandaag = DateTime.Now;
            string[] ingelezenReservaties = { };
            string namenReservaties = "";
            int tellerNamen = 1;
            string aantalTafelsOpDatum = "";
            int lengteSubtring = 0;
            int totaleLengsteString = 0;
            string[] arrAantalTafelsopDatum = { };
            int tellerVerwijderingTafel = 0;
            string fileName = "BezettingperDatum.txt";

            Console.Clear();

            if (File.Exists("Reservaties.txt"))
            {

                ingelezenFile[0] = File.ReadAllText("Reservaties.txt");

                if (string.IsNullOrWhiteSpace(ingelezenFile[0]))
                {
                    Console.WriteLine("Er zijn geen reserveringen om te verwijderen." + Environment.NewLine + "Druk op enter om terug te keren naar het hoofdmenu.");
                    Console.ReadLine();
                    MainMenu();
                }

                InlezenReservatieBestand2(ref Namen, ref ReservatieDagen, ref KeuzeLunchDiner, ref AantalPersonen, ref toegewezenTafels);
            }
            else
            {
                Console.WriteLine("Er zijn geen reserveringen om te verwijderen." + Environment.NewLine + "Druk op enter om terug te keren naar het hoofdmenu.");
                Console.ReadLine();
                MainMenu();
            }


            string[] arrNamen = Namen.Split(';', Namen.Length);
            string[] arrReservatieDagen = ReservatieDagen.Split(';', ReservatieDagen.Length);
            string[] arrKeuzeLunchDiner = KeuzeLunchDiner.Split(';', KeuzeLunchDiner.Length);
            string[] arrAantalPersonen = AantalPersonen.Split(';', AantalPersonen.Length);
            string[] arrToegewezenTafels = toegewezenTafels.Split(';', toegewezenTafels.Length);

            Console.WriteLine("Op welke datum wilt u een reservatie wijzigen? (dd/mm/yyyy)");

            while (!DateTime.TryParse(Console.ReadLine(), out reservatieDag))
            {
                Console.WriteLine("Graag in het format dd/mm/yyyy).");
            }

            while (reservatieDag.CompareTo(vandaag) < 0)
            {
                Console.WriteLine("De ingegeven datum ligt in het verleden. Gelieve opnieuw een datum in te geven.");
                while (!DateTime.TryParse(Console.ReadLine(), out reservatieDag))
                {
                    Console.WriteLine("Graag in het format dd/mm/yyyy.");
                }
            }

            dag = reservatieDag.ToString("dd/MM/yyyy");

            using (StreamReader reader = new StreamReader("BezettingperDatum.txt"))
            {
                teWijzigenLijn = 0;
                for (int j = 0; j < File.ReadAllLines("BezettingperDatum.txt").Length; j++)
                {
                    ingelezenLijn = reader.ReadLine();
                    matchDatum = ingelezenLijn;
                    teWijzigenLijnOrigineel++;

                    if (!(ingelezenLijn.IndexOf(';') == -1))
                    {
                        matchDatum = ingelezenLijn.Substring(0, ingelezenLijn.IndexOf(";"));
                    }
                    if (matchDatum == dag)
                    {
                        break;
                    }
                }
            }

            aantalTafelsOpDatum = ingelezenLijn;

            if (!(aantalTafelsOpDatum == matchDatum))
            {
                lengteSubtring = Convert.ToInt32(aantalTafelsOpDatum.Substring(0, aantalTafelsOpDatum.IndexOf(';')).Length + 1);
                totaleLengsteString = aantalTafelsOpDatum.Length - lengteSubtring;
                aantalTafelsOpDatum = aantalTafelsOpDatum.Substring(lengteSubtring, totaleLengsteString);
                arrAantalTafelsopDatum = aantalTafelsOpDatum.Split(";");
            }
            else
            {
                Console.WriteLine(Environment.NewLine + "Er zijn geen reserveringen om te verwijderen." + Environment.NewLine + "Druk op enter om terug te keren naar het hoofdmenu.");
                Console.ReadLine();
                MainMenu();
            }

            Console.Clear();

            Console.WriteLine("Welke Reservering wilt u verwijderen?" + Environment.NewLine);

            for (int i = 0; i < arrAantalTafelsopDatum.Length; i++)
            {
                for (int j = 0; j < arrAantalPersonen.Length; j++)
                {
                    if ((arrAantalTafelsopDatum[i] == arrToegewezenTafels[j]) && (dag == arrReservatieDagen[j]))
                    {
                        namenReservaties += tellerNamen + ") " + arrNamen[j].PadRight(12) + " " + arrAantalPersonen[j] + "P" + Environment.NewLine;
                        tellerNamen++;
                    }
                }
            }

            Console.WriteLine(namenReservaties);

            while (!int.TryParse(Console.ReadLine(), out keuzeVerwijdering))
            {
                Console.WriteLine("Graag een cijfer ingeven.");
            }
            while (keuzeVerwijdering > arrNamen.Length)
            {
                Console.WriteLine("Graag een juiste keuzemenu aanduiden.");
                while (!int.TryParse(Console.ReadLine(), out keuzeVerwijdering))
                {
                    Console.WriteLine("Graag een cijfer ingeven.");
                }
            }

            ingelezenLijn = dag;

            for (int i = 0; i < arrAantalTafelsopDatum.Length; i++)
            {
                if (arrAantalTafelsopDatum[tellerVerwijderingTafel] != arrAantalTafelsopDatum[keuzeVerwijdering - 1])
                {
                    ingelezenLijn += ";" + arrAantalTafelsopDatum[tellerVerwijderingTafel];
                    tellerVerwijderingTafel++;
                }
                else
                {
                    tellerVerwijderingTafel++;
                }
            }

            lineChanger(ingelezenLijn, fileName, teWijzigenLijnOrigineel);

            Console.Clear();

            Namen = "";
            ReservatieDagen = "";
            KeuzeLunchDiner = "";
            AantalPersonen = "";
            toegewezenTafels = "";

            string[] arrNamenNieuw = new string[arrNamen.Length - 1];
            string[] arrReservatieDagenNieuw = new string[arrNamen.Length - 1];
            string[] arrKeuzeLunchDinerNieuw = new string[arrNamen.Length - 1];
            string[] arrAantalPersonenNieuw = new string[arrNamen.Length - 1];
            string[] arrToegewezenTafelsNieuw = new string[arrNamen.Length - 1];

            int teller = 0;

            for (int i = 0; i < arrAantalPersonen.Length; i++)
            {
                if (i == keuzeVerwijdering - 1)
                {
                    //
                }
                else
                {
                    arrNamenNieuw[teller] = arrNamen[i];
                    arrReservatieDagenNieuw[teller] = arrReservatieDagen[i];
                    arrKeuzeLunchDinerNieuw[teller] = arrKeuzeLunchDiner[i];
                    arrAantalPersonenNieuw[teller] = arrAantalPersonen[i];
                    arrToegewezenTafelsNieuw[teller] = arrToegewezenTafels[i];

                    teller++;
                }
            }

            Namen = string.Join(";", arrNamenNieuw);
            ReservatieDagen = string.Join(";", arrReservatieDagenNieuw);
            KeuzeLunchDiner = string.Join(";", arrKeuzeLunchDinerNieuw);
            AantalPersonen = string.Join(";", arrAantalPersonenNieuw);
            toegewezenTafels = string.Join(";", arrToegewezenTafelsNieuw);


            using (StreamWriter writer = new StreamWriter("Reservaties.txt"))
            {
                writer.WriteLine(Namen);
                writer.WriteLine(ReservatieDagen);
                writer.WriteLine(KeuzeLunchDiner);
                writer.WriteLine(AantalPersonen);
                writer.WriteLine(toegewezenTafels);
            }

            MainMenu();
        }
        static void Reserveringsoverzicht()
        {
            string Namen = "";
            string ReservatieDagen = "";
            string KeuzeLunchDiner = "";
            string AantalPersonen = "";
            string toegewezenTafels = "";
            string[] ingelezenFile = new string[1];
            DateTime reservatieDag;
            int teWijzigenLijnOrigineel = 0;
            string ingelezenLijn = "";
            string matchDatum = "";
            string dag = "";
            DateTime vandaag = DateTime.Now;
            string[] ingelezenReservaties = { };
            string namenReservaties = "";
            int tellerNamen = 1;
            string aantalTafelsOpDatum = "";
            int lengteSubtring = 0;
            int totaleLengsteString = 0;
            string[] arrAantalTafelsopDatum = { };


            Console.Clear();

            if (File.Exists("Reservaties.txt"))
            {

                ingelezenFile[0] = File.ReadAllText("Reservaties.txt");

                if (string.IsNullOrWhiteSpace(ingelezenFile[0]))
                {
                    Console.WriteLine("Er zijn geen reserveringen." + Environment.NewLine + "Druk op enter om terug te keren naar het hoofdmenu.");
                    Console.ReadLine();
                    MainMenu();
                }

                InlezenReservatieBestand2(ref Namen, ref ReservatieDagen, ref KeuzeLunchDiner, ref AantalPersonen, ref toegewezenTafels);
            }
            else
            {
                Console.WriteLine("Er zijn geen reserveringen." + Environment.NewLine + "Druk op enter om terug te keren naar het hoofdmenu.");
                Console.ReadLine();
                MainMenu();
            }


            string[] arrNamen = Namen.Split(';', Namen.Length);
            string[] arrReservatieDagen = ReservatieDagen.Split(';', ReservatieDagen.Length);
            string[] arrKeuzeLunchDiner = KeuzeLunchDiner.Split(';', KeuzeLunchDiner.Length);
            string[] arrAantalPersonen = AantalPersonen.Split(';', AantalPersonen.Length);
            string[] arrToegewezenTafels = toegewezenTafels.Split(';', toegewezenTafels.Length);

            Console.WriteLine("Op welke datum wilt u een overzicht van uw reserveringen? (dd/mm/yyyy)");

            while (!DateTime.TryParse(Console.ReadLine(), out reservatieDag))
            {
                Console.WriteLine("Graag in het format dd/mm/yyyy).");
            }

            while (reservatieDag.CompareTo(vandaag) < 0)
            {
                Console.WriteLine("De ingegeven datum ligt in het verleden. Gelieve opnieuw een datum in te geven.");
                while (!DateTime.TryParse(Console.ReadLine(), out reservatieDag))
                {
                    Console.WriteLine("Graag in het format dd/mm/yyyy.");
                }
            }

            dag = reservatieDag.ToString("dd/MM/yyyy");

            using (StreamReader reader = new StreamReader("BezettingperDatum.txt"))
            {
                for (int j = 0; j < File.ReadAllLines("BezettingperDatum.txt").Length; j++)
                {
                    ingelezenLijn = reader.ReadLine();
                    matchDatum = ingelezenLijn;
                    teWijzigenLijnOrigineel++;

                    if (!(ingelezenLijn.IndexOf(';') == -1))
                    {
                        matchDatum = ingelezenLijn.Substring(0, ingelezenLijn.IndexOf(";"));
                    }
                    if (matchDatum == dag)
                    {
                        break;
                    }
                }
            }

            aantalTafelsOpDatum = ingelezenLijn;

            if (!(aantalTafelsOpDatum == matchDatum))
            {
                lengteSubtring = Convert.ToInt32(aantalTafelsOpDatum.Substring(0, aantalTafelsOpDatum.IndexOf(';')).Length + 1);
                totaleLengsteString = aantalTafelsOpDatum.Length - lengteSubtring;
                aantalTafelsOpDatum = aantalTafelsOpDatum.Substring(lengteSubtring, totaleLengsteString);
                arrAantalTafelsopDatum = aantalTafelsOpDatum.Split(";");
            }
            else
            {
                Console.WriteLine(Environment.NewLine + "Er zijn geen reserveringen." + Environment.NewLine + "Druk op enter om terug te keren naar het hoofdmenu.");
                Console.ReadLine();
                MainMenu();

            }

            Console.Clear();

            Console.WriteLine("Op datum " + dag + " zijn er volgende reserveringen ingepland:" + Environment.NewLine);

            for (int i = 0; i < arrAantalTafelsopDatum.Length; i++)
            {
                for (int j = 0; j < arrAantalPersonen.Length; j++)
                {
                    if ((arrAantalTafelsopDatum[i] == arrToegewezenTafels[j]) && (dag == arrReservatieDagen[j]))
                    {
                        namenReservaties += tellerNamen + ") " + arrNamen[j].PadRight(12) + " " + arrAantalPersonen[j] + "P" + Environment.NewLine;
                        tellerNamen++;
                    }
                }
            }

            Console.WriteLine(namenReservaties + Environment.NewLine + "Druk op enter om terug te keren naar het hoofdmenu.");
            Console.ReadLine();
            MainMenu();
        }

        // Tafelbeheer 
        static void TafelBeheer()
        {
            Console.Clear();

            string logo =
@"
  _______  _______  _______ _________ _______  _______  _______  _______          _________ _______  _______
 (  ____ )(  ____ \(  ____ \\__   __/(  ___  )(  ____ \(  ____ \(  ____ )|\     /|\__   __/(  ____ \(  ____ \
 | (    )|| (    \/| (    \/   ) (   | (   ) || (    \/| (    \/| (    )|| )   ( |   ) (   | (    \/| (    \/
 | (____)|| (__    | (_____    | |   | |   | || (_____ | (__    | (____)|| |   | |   | |   | |      | (__
 |     __)|  __)   (_____  )   | |   | |   | |(_____  )|  __)   |     __)( (   ) )   | |   | |      |  __)
 | (\ (   | (            ) |   | |   | |   | |      ) || (      | (\ (    \ \_/ /    | |   | |      | (
 | ) \ \__| (____/\/\____) |   | |   | (___) |/\____) || (____ /\| ) \ \__ \   /  ___) (___| (____ /\| (____ /\
 |/   \__ / (_______ /\_______)   )_((_______)\_______)(_______ /|/   \__ / \_/   \_______/(_______ /(_______ /
           ";

            Console.WriteLine(logo + Environment.NewLine);

            string[] subMenuTafelBeheer = { "Tafels toevoegen", "Tafeloverzicht", "Terug naar hoofdmenu" };
            int keuzeMenu;

            Console.WriteLine("Gelieve uw keuze te maken uit onderstaande: (Druk het bijpassende cijfer in)" + Environment.NewLine);

            for (int i = 0; i < subMenuTafelBeheer.Length; i++)
            {
                Console.WriteLine((i + 1) + ") " + subMenuTafelBeheer[i]);
            }

            while (!int.TryParse(Console.ReadLine(), out keuzeMenu))
            {
                Console.WriteLine("Graag een correct cijfer in te geven.");
            }

            while (keuzeMenu > subMenuTafelBeheer.Length)
            {
                Console.WriteLine("Graag een keuze maken uit de bestaande cijfers");

                while (!int.TryParse(Console.ReadLine(), out keuzeMenu))
                {
                    Console.WriteLine("Graag een correct cijfer in te geven.");
                }
            }

            switch (keuzeMenu)
            {

                case (int)KeuzeSubMenuTafels.Tafelaanmaken + 1:
                    TafelAanmaken();
                    break;
                case (int)KeuzeSubMenuTafels.Tafeloverzicht + 1:
                    Tafeloverzicht();
                    break;
                case (int)KeuzeSubMenuTafels.Hoofdmenu + 1:
                    MainMenu();
                    break;

                default:
                    break;
            }

            Console.ReadLine();
        }
        static void TafelAanmaken()
        {
            string[] tafelNaam = new string[1];
            string naamTafel;
            int aantalPersoon;
            string[] keuzeMenuAantalPersonen = { "Tafel voor 1 a 2 personen", "Tafel voor 3 a 4 personen", "Tafel voor 5 a 6 personen", "Tafel voor 7 a 8 personen" };
            string[] arrReedsAangemaakteTafels = { };
            string[] arrAantalPersonenTafels = { };
            string namenTafels = "";
            string aantalPersonen = "";
            string reedsAangemaakteTafels = "";
            int lengteAantalTafels = 0;


            Console.Clear();

            if (File.Exists("Tafelbestand.txt"))
            {
                InlezenTafelBestand(ref namenTafels, ref aantalPersonen);

                arrReedsAangemaakteTafels = namenTafels.Split(";");
                arrAantalPersonenTafels = aantalPersonen.Split(";");
                lengteAantalTafels = arrAantalPersonenTafels.Length;

                Console.WriteLine("De volgende tafels zijn reeds aangemaakt:" + Environment.NewLine);

                for (int i = 0; i < arrReedsAangemaakteTafels.Length; i++)
                {
                    reedsAangemaakteTafels += (i + 1) + ") " + arrReedsAangemaakteTafels[i] + " " + arrAantalPersonenTafels[i] + "P" + Environment.NewLine;
                }

                Console.WriteLine(reedsAangemaakteTafels);
            }
            else
            {
                Console.WriteLine("Er zijn nog geen tafels aangemaakt. Klik op enter om uw eerste tafel toe te voegen.");
                Console.ReadLine();
            }

            Console.WriteLine("Hoe wilt u uw nieuwe tafel benoemen?" + Environment.NewLine + "Zorg voor een oplopende nummering bv. Tafel1, Tafel2,... .");
            naamTafel = Console.ReadLine();

            if (File.Exists("Tafelbestand.txt"))
            {
                while (naamTafel.CompareTo(arrReedsAangemaakteTafels[lengteAantalTafels - 1]) <= 0)
                {
                    Console.WriteLine("UW tafelnummering klopt niet, Gelieve een opeenvolgende nummering te gebruiken.");
                    naamTafel = Console.ReadLine();
                }
            }

            Console.WriteLine(Environment.NewLine + "Voor hoeveel personen is uw nieuwe tafel?");

            for (int i = 0; i < keuzeMenuAantalPersonen.Length; i++)
            {
                Console.WriteLine((i + 1) + ") " + keuzeMenuAantalPersonen[i]);
            }

            while (!int.TryParse(Console.ReadLine(), out aantalPersoon))
            {
                Console.WriteLine("Graag een correct cijfer in te voeren.");
            }
            while (aantalPersoon > 4)
            {
                Console.WriteLine(Environment.NewLine + "u heeft een cijfer groter dan 4 ingegeven. Gelieve opnieuw een keuze te maken.");
                while (!int.TryParse(Console.ReadLine(), out aantalPersoon))
                {
                    Console.WriteLine("Graag een correct cijfer in te voeren.");
                }
            }

            switch (aantalPersoon)
            {
                case 1:
                    aantalPersoon = 2;
                    break;
                case 2:
                    aantalPersoon = 4;
                    break;
                case 3:
                    aantalPersoon = 6;
                    break;
                case 4:
                    aantalPersoon = 8;
                    break;

                default:
                    break;

            }

            string tussenTeken = ";";
            string fileName = "Tafelbestand.txt";

            using (StreamWriter writer = new StreamWriter(fileName))
            {
                if (namenTafels == "")
                {
                    tussenTeken = "";
                }

                writer.WriteLine(namenTafels + tussenTeken + naamTafel);
                writer.WriteLine(aantalPersonen + tussenTeken + aantalPersoon);

            }
            Console.Clear();
            MainMenu();

        }
        static void Tafeloverzicht()
        {
            Console.Clear();

            string[] tafelNaam = new string[1];
            string[] arrReedsAangemaakteTafels = { };
            string[] arrAantalPersonenTafels = { };
            string namenTafels = "";
            string aantalPersonen = "";
            string reedsAangemaakteTafels = "";
            int lengteAantalTafels = 0;


            if (File.Exists("Tafelbestand.txt"))
            {
                InlezenTafelBestand(ref namenTafels, ref aantalPersonen);

                arrReedsAangemaakteTafels = namenTafels.Split(";");
                arrAantalPersonenTafels = aantalPersonen.Split(";");
                lengteAantalTafels = arrAantalPersonenTafels.Length;

                Console.WriteLine("De volgende tafels zijn reeds aangemaakt:" + Environment.NewLine);

                for (int i = 0; i < arrReedsAangemaakteTafels.Length; i++)
                {
                    reedsAangemaakteTafels += (i + 1) + ") " + arrReedsAangemaakteTafels[i].PadRight(12) + " " + arrAantalPersonenTafels[i] + "P" + Environment.NewLine;
                }

                Console.WriteLine(reedsAangemaakteTafels + Environment.NewLine + "Druk op enter om terug te keren naar het hoofdmenu.");
            }
            else
            {
                Console.WriteLine("Er zijn nog geen tafels aangemaakt. Klik op enter om naar Tafelbeheer terug te keren.");
                Console.ReadLine();
            }

            Console.ReadLine();
            TafelBeheer();
        }
        static void InlezenTafelBestand(ref string dataNamenTafels, ref string dataAantalPersonen)
        {
            using (StreamReader reader = new StreamReader("Tafelbestand.txt"))
            {
                dataNamenTafels = reader.ReadLine();
                dataAantalPersonen = reader.ReadLine();
            }

        }
        static void TafelBezetting(ref string dataDatum, ref string dataAantalpersoon)
        {
            DateTime vandaag = DateTime.Now;
            DateTime eindeVanHetJaar = new DateTime(DateTime.Today.Year + 1, 1, 1).AddDays(-1);
            int verschilAantalDagen;
            DateTime[] alleResteredenDagen = new DateTime[0];
            string[] ingelezenFile = { "" };
            String[] ingelezenLijn = new string[0];

            verschilAantalDagen = (eindeVanHetJaar.Date - vandaag.Date).Days;
            Array.Resize(ref alleResteredenDagen, verschilAantalDagen);
            Array.Resize(ref ingelezenLijn, verschilAantalDagen);

            if (!(File.Exists("BezettingperDatum.txt")))
            {
                using (StreamWriter writer = new StreamWriter("BezettingperDatum.txt"))
                {
                    for (int j = 0; j <= verschilAantalDagen - 1; j++)
                    {

                        alleResteredenDagen[j] = vandaag.AddDays(j);
                        writer.WriteLine(alleResteredenDagen[j].ToString("dd/MM/yyyy"));
                    }
                }
            }

        }
        static void Tafelkeuze(ref string dataDag, ref string dataAantalPersoon, ref int dataTeWijzigenLijn, ref string dataIngelezenLijn, ref string dataToegewezenTafel)
        {
            string dag = dataDag;
            string aantalPersoon = dataAantalPersoon;
            DateTime vandaag = DateTime.Now;
            DateTime eindeVanHetJaar = new DateTime(DateTime.Today.Year + 1, 1, 1).AddDays(-1);
            DateTime[] alleResteredenDagen = new DateTime[0];
            string[] ingelezenFile = { "" };
            string[] tafelNaam = new string[1];
            string namenTafels = "";
            string aantalPersonen = "";
            string[] aantalTafels = { };
            string[] aantalTafelsNieuw;
            string[] aantalPersonenKeuze = { };
            string[] aantalPersonenKeuzeNieuw;
            string ingelezenLijn = dataIngelezenLijn;
            string[] ingelezenKeuzeLijn = { };
            int teWijzigenLijn = dataTeWijzigenLijn;
            string matchDatum = "";
            string aantalTafelsOpDatum = "";
            string[] arrAantalTafelsopDatum;
            int tellerAantalTafels = 0;
            int tellerAantalTafelsNieuw = 0;
            int lengteSubtring = 0;
            int totaleLengsteString = 0;
            int lengteNieuweTafels;
            int lengte = 0;
            bool tafelVrij = false;
            string toegewezenTafel = "";


            if (File.Exists("Tafelbestand.txt"))
            {
                InlezenTafelBestand(ref namenTafels, ref aantalPersonen);
            }
            else
            {
                Console.WriteLine(Environment.NewLine + "U moet eerst een tafelbestand aanmaken." + Environment.NewLine + "Druk op enter om terug te keren naar het hoofdmenu.");
                Console.ReadLine();
                MainMenu();
            }

            aantalTafels = namenTafels.Split(";");
            aantalPersonenKeuze = aantalPersonen.Split(";");

            TafelBezetting(ref dag, ref aantalPersoon);

            for (int i = 0; i < Convert.ToInt32(aantalTafels.Length); i++)
            {
                if (aantalPersoon == aantalPersonenKeuze[i])
                {
                    TafelBezetting(ref dag, ref aantalPersoon);

                    using (StreamReader reader = new StreamReader("BezettingperDatum.txt"))
                    {
                        teWijzigenLijn = 0;
                        for (int j = 0; j < File.ReadAllLines("BezettingperDatum.txt").Length; j++)
                        {
                            ingelezenLijn = reader.ReadLine();
                            matchDatum = ingelezenLijn;
                            teWijzigenLijn++;
                            tellerAantalTafels = 0;

                            if (!(ingelezenLijn.IndexOf(';') == -1))
                            {
                                matchDatum = ingelezenLijn.Substring(0, ingelezenLijn.IndexOf(";"));
                            }
                            if (matchDatum == dag)
                            {
                                if ((ingelezenLijn.IndexOf(';') == -1))
                                {
                                    ingelezenLijn += ";" + aantalTafels[i];
                                    toegewezenTafel = aantalTafels[i];
                                    break;
                                }
                                else
                                {
                                    aantalTafelsOpDatum = ingelezenLijn;
                                    lengteSubtring = Convert.ToInt32(aantalTafelsOpDatum.Substring(0, aantalTafelsOpDatum.IndexOf(';')).Length + 1);
                                    totaleLengsteString = aantalTafelsOpDatum.Length - lengteSubtring;
                                    aantalTafelsOpDatum = aantalTafelsOpDatum.Substring(lengteSubtring, totaleLengsteString);
                                    arrAantalTafelsopDatum = aantalTafelsOpDatum.Split(";");
                                    Array.Sort(arrAantalTafelsopDatum);
                                    lengte = arrAantalTafelsopDatum.Length - 1;
                                    lengteNieuweTafels = Convert.ToInt32(aantalTafels.Length - arrAantalTafelsopDatum.Length);
                                    aantalTafelsNieuw = new string[lengteNieuweTafels];
                                    aantalPersonenKeuzeNieuw = new string[lengteNieuweTafels];


                                    for (int m = 0; m < aantalTafels.Length; m++)
                                    {
                                        if (tellerAantalTafels < arrAantalTafelsopDatum.Length)
                                        {
                                            if (aantalTafels[m] == arrAantalTafelsopDatum[tellerAantalTafels])
                                            {
                                                tellerAantalTafels++;

                                            }
                                            else
                                            {
                                                aantalTafelsNieuw[tellerAantalTafelsNieuw] = aantalTafels[m];
                                                aantalPersonenKeuzeNieuw[tellerAantalTafelsNieuw] = aantalPersonenKeuze[m];
                                                tellerAantalTafelsNieuw++;
                                            }
                                        }

                                        else
                                        {
                                            if (aantalPersoon == aantalPersonenKeuze[m])
                                            {
                                                aantalTafelsNieuw[tellerAantalTafelsNieuw] = aantalTafels[m];
                                                aantalPersonenKeuzeNieuw[tellerAantalTafelsNieuw] = aantalPersonenKeuze[m];
                                                tellerAantalTafelsNieuw++;
                                            }
                                        }

                                    }

                                    for (int k = 0; k < lengteNieuweTafels; k++)
                                    {
                                        if (aantalPersoon == aantalPersonenKeuzeNieuw[k])
                                        {
                                            ingelezenLijn += ";" + aantalTafelsNieuw[k];
                                            toegewezenTafel = aantalTafelsNieuw[k];
                                            tafelVrij = true;
                                            break;
                                        }

                                        else
                                        {
                                            tafelVrij = false;
                                        }

                                    }
                                    if (tafelVrij == false && aantalTafelsNieuw.Length != 0)
                                    {
                                        Console.WriteLine("Er zijn geen tafels meer vrij voor het aantal personen." + Environment.NewLine +
                                                            "Gelieve contact op te nemen met het restaurant" + Environment.NewLine +
                                                            "Druk op enter om terug te keren naar het hoofdmenu.");
                                        Console.ReadLine();
                                        teWijzigenLijn = -1;
                                        break;
                                    }
                                    if (aantalTafelsNieuw.Length == 0)
                                    {
                                        Console.WriteLine("We zijn volzet." + Environment.NewLine +
                                                            "Gelieve contact op te nemen met het restaurant" + Environment.NewLine +
                                                            "Druk op enter om terug te keren naar het hoofdmenu.");
                                        Console.ReadLine();
                                        teWijzigenLijn = -1;
                                    }

                                }
                                break;
                            }

                        }
                        break;
                    }

                }
            }
            dataTeWijzigenLijn = teWijzigenLijn;
            dataIngelezenLijn = ingelezenLijn;
            dataToegewezenTafel = toegewezenTafel;
        }

        // Divers
        static void lineChanger(string newText, string fileName, int line_to_edit)
        {
            string[] arrLine = File.ReadAllLines(fileName);
            arrLine[line_to_edit - 1] = newText;
            File.WriteAllLines(fileName, arrLine);
        }
    }
}
